import matplotlib.pyplot as plt
import numpy as np

#Isabela Behring Avelar - 1724673
#Adriana Jhoselin Castelo Hinojosa - 1711644
#Ana Beatriz Guedes Goncalves - 1857177
#Geovana Córdova - 1857622
inicial = 0 
final = 90 


theta = np.array(np.linspace(inicial, final, num=100)) 
sind_theta = np.array(np.sin(np.deg2rad(theta)))
cosd_theta = np.array(np.cos(np.deg2rad(theta)))


Ox = ( (3/4.0) *sind_theta) * ((3*(cosd_theta))-2)
plt.plot(Ox) 
print("Ox")
plt.show()

Oy = (1/4.0) * (((3*(cosd_theta))-1) ** 2) 
plt.plot(Oy) 
print("Oy")

plt.show()


